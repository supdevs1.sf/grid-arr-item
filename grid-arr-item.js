import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `grid-arr-item`
 * Item for Grid Array listing
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class GridArrItem extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          text-align: center;
        }

        .link {
          transition: opacity 0.5s;
          font-family: 'Abril Fatface', cursive;
          font-size: 2em;
          color: #0a3142;
        }        

      </style>

      <div>
        <slot>No content given!</slot>
      </div>

      <h2 class="link">[[header]]</h2>
    `;
  }
  static get properties() {
    return {
      header: {
        type: String,
        value: 'header',
      },
    };
  }
}

window.customElements.define('grid-arr-item', GridArrItem);
